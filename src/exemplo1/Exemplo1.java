/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Gabriel Souza de Paula
 */
public class Exemplo1  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");

            //Cria cada thread com um novo runnable selecionado

            ExecutorService threadExecutor = Executors.newCachedThreadPool();

            Integer totalCarros = 4;

            Controlador controlador = new Controlador();

            for (int i=1; i<=totalCarros; i++){
                Thread t20s = new Thread(new Carro(i, controlador));
                threadExecutor.execute(t20s);
            }
            threadExecutor.shutdown();

            try {
                threadExecutor.awaitTermination(1, TimeUnit.DAYS);
                controlador.printRank();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


        
}
