package exemplo1;

import java.util.Map;
import java.util.Random;

/**
 * Created by Gabriel de Paula on 29/09/2017.
 */
public class Carro implements Runnable{

    private Integer idCarro;

    private Controlador controlador;

    public Carro(Integer idCarro, Controlador controlador) {
        this.idCarro = idCarro;
        this.controlador = controlador;
    }

    @Override
    public void run() {

        for (int i = 0; i < 5; i ++){
            try {
                int tempoVolta = new Random().nextInt(1000);
                Thread.sleep(tempoVolta);
                controlador.addInMapTempo(idCarro, tempoVolta);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public Integer getIdCarro() {
        return idCarro;
    }

}
