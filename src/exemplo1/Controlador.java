package exemplo1;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gabri on 29/09/2017.
 */
public class Controlador {

    private Map<Integer, Integer> mapaDeTempoCarros;

    private int tempoCarroVencedor;

    public Map<Integer, Integer> getMapaDeTempoCarros() {
        return mapaDeTempoCarros;
    }

    public void addInMapTempo(Integer idCarro, Integer tempo){

        if(mapaDeTempoCarros == null){
            mapaDeTempoCarros = new HashMap<>();
        }

        if(mapaDeTempoCarros.containsKey(idCarro)){
            mapaDeTempoCarros.put(idCarro, mapaDeTempoCarros.get(idCarro) + tempo);
        } else {
            mapaDeTempoCarros.put(idCarro, tempo);
        }
    }


    public void printRank(){
        Integer menorTempo = 99999999;
        Integer idCarroMenorTempo = -1;
        for(Integer carro : getMapaDeTempoCarros().keySet()){
            System.out.println("Carro " + carro + " fez " + mapaDeTempoCarros.get(carro) + " ms.");
            Integer tempoAtual = mapaDeTempoCarros.get(carro);

            if(tempoAtual < menorTempo){
                idCarroMenorTempo = carro;
                menorTempo = mapaDeTempoCarros.get(carro);
            }

        }

        if(idCarroMenorTempo > -1){
            System.out.println("O carro " + idCarroMenorTempo + " ganhou.");
        }

    }

    public int getTempoCarroVencedor() {
        return tempoCarroVencedor;
    }

    public void setTempoCarroVencedor(int tempoCarroVencedor) {
        this.tempoCarroVencedor = tempoCarroVencedor;
    }
}
